#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    float total = 0, nilai, rata;
    int banyakData = 0;
    int i = 1;

    do {
      cout << "Masukan Data ke-" << i << ": ";
      cin >> nilai;
      total += nilai;
      banyakData++;
      i++;
    } while (nilai != 0);

    banyakData--;// Mengurangi 1 karena input terakhir adalah 0 yang tidak ingin dihitung

    cout << "\nBanyaknya Data : " << banyakData << endl;
    cout << "\Total Nilai Data : " << fixed << setprecision(2) << total << endl;

    if (banyakData != 0) {
        rata = total / banyakData;
        cout << "\Rata-rata nilai data: " << fixed << setprecision(2) << rata << endl;
    } else {
        cout << "Tidak bisa mendapatkan rata-rata karena tidak ada data.\n";
    }

    return 0;
}
  
                
                                         
